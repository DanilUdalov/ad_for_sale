# frozen_string_literal: true

require_relative 'boot'

require 'rails/all'

Bundler.require(*Rails.groups)

module AdForSale
  class Application < Rails::Application
    config.action_mailer.smtp_settings = {
      address: 'smtp.gmail.com',
      port: 587,
      domain: 'gmail.com',
      user_name: 'gtafan51@gmail.com',
      password: '80953540354udalov',
      authentication: :plain,
      enable_starttls_auto: true
    }
    config.action_mailer.default_url_options = {
      host: 'localhost::3000'
    }
  end
end
