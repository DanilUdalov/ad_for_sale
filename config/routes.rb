# frozen_string_literal: true

Rails.application.routes.draw do
  get 'my_ads', to: 'dashboard#index', as: 'my_ads'
  get 'banned', to: 'welcome#banned', as: 'banned'
  get 'chat_room', to: 'welcome#chat_user', as: 'chat_room'

  get 'contact', to: 'messages#new', as: 'contact'
  post 'contact', to: 'messages#create'

  devise_for :users, controllers: { registrations: 'registrations', omniauth_callbacks: "users/omniauth_callbacks" }

  resources :users do
    resources :profiles, only: %i[edit update show]
    resources :user_reviews
  end

  resources :chat_rooms, only: [:create] do
    member do
      post :close
    end
    resources :latters, only: [:create]
  end

  resources :ads do
    get :favourite, on: :member
  end

  resources :categories do
    resources :ads
  end

  root 'welcome#index'

  namespace :admin do
    root 'users#index'
    resources :users do
      get :change_permission, on: :member
      get :ad_to_black_list, on: :member
    end
    resources :ads do
      get :ad_approval, on: :member
    end
    resources :categories
  end
end
