SocialShareButton.configure do |config|
  config.allow_sites = %w(vkontakte facebook google_plus twitter telegram)
end
