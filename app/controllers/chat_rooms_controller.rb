# frozen_string_literal: true

class ChatRoomsController < ApplicationController
  def create
    @chat_room = ChatRoom.get(current_user.id, params[:user_id])
    
    add_to_chat_rooms unless conversated?

    respond_to do |format|
      format.js
    end
  end

  private

  def add_to_chat_rooms
    session[:chat_rooms] ||= []
    session[:chat_rooms] << @chat_room.id
  end

  def conversated?
    session[:chat_rooms].include?(@chat_room.id)
  end
end
