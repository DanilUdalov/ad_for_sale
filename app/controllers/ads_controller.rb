# frozen_string_literal: true

class AdsController < ApplicationController
  before_action :ads_resourse,
                only: %i[show edit update destroy favourite]
  before_action :ads_views_inc, only: %i[show]
  before_action :require_user, only: %i[new create edit update destroy]
  before_action :require_owner, only: %i[edit update destroy]

  def new
    @ad = Ad.new
    @categories = Category.all
  end

  def show; end

  def edit; end

  def index
    @ads =
      if params[:search]
        Ad.published.search(params[:search])
      else
        Ad.published
      end.page(params[:page])
  end

  def update
    if @ad.update(ads_params)
      redirect_to @ad
    else
      render :edit
    end
  end

  def create
    @ad = current_user.ads.create(ads_params)
    if @ad.save
      redirect_to my_ads_path, notice: 'Wait for publication approval!'
    else
      render :new
    end
  end

  def destroy
    @ad.destroy!
  end

  def favourite
    current_user.favourite_ads.manage(@ad)
  end

  private

  def ads_resourse
    @ad = Ad.find(params[:id])
    if params[:category_id]
      @category = Category.find params[:category_id]
      @ads = @category.ads
    else
      @ads = Ad.all
    end
  end

  def ads_params
    params.require(:ad).permit(:name, :price, :description,
                               :image_ad, :published, category_ids: [],
                                                      images_attributes: %i[url imageable _destroy])
  end

  def ads_views_inc
    @ad.ads_views.create(user_id: current_user.id) if user_signed_in?
  end

  def require_owner
    redirect_to root_path unless current_user == @ad.user
  end
end
