# frozen_string_literal: true

class Admin::UsersController < AdminsController
  before_action :user_resource, only: %i[destroy change_permission ad_to_black_list]

  def index
    @users = User.order(:id)
    @banned_users = User.where(banned: true)
  end

  def destroy
    @user.destroy!
  end

  def change_permission
    @user.update(admin: !@user.admin)
    redirect_to :back
  end

  def ad_to_black_list
    @user.update(banned: !@user.banned)
    redirect_to :back
  end

  private

  def user_resource
    @user = User.find(params[:id])
  end
end
