# frozen_string_literal: true

class Admin::CategoriesController < AdminsController
  before_action :category_resource, only: [:destroy]

  def new
    @category = Category.new
    @category.subcategories.build
  end

  def index
    @categories = Category.includes(:subcategories)
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_to admin_categories_path
    else
      render :new
    end
  end

  def destroy
    @category.destroy!
  end

  private

  def category_params
    params.require(:category).permit(:name, subcategories_attributes: %i[name parent_category_id _destroy])
  end

  def category_resource
    @category = Category.find(params[:id])
  end
end
