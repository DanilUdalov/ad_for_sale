# frozen_string_literal: true

class Admin::AdsController < AdminsController
  before_action :ads_resourse, only: %i[destroy ad_approval]

  def index
    @ads_aproval = Ad.published.page(params[:page])
    @ads_not_aproval = Ad.not_published
  end

  def ad_approval
    @ad.update(published: !@ad.published)
    redirect_to :back
  end

  private

  def ads_resourse
    @ad = Ad.find(params[:id])
    if params[:category_id]
      @category = Category.find params[:category_id]
      @ads = @category.ads
    else
      @ads = Ad.all
    end
  end

  def ads_params
    params.require(:ad).permit(:name, :price, :description,
                               :image_ad, :published, category_ids: [])
  end
end
