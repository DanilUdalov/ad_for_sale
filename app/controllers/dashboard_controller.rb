# frozen_string_literal: true

class DashboardController < ApplicationController
  before_action :require_user, only: [:index]

  def index
    @ads = current_user.ads
    @fav_ads = current_user.favourite_ads
  end
end
