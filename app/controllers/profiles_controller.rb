# frozen_string_literal: true

class ProfilesController < ApplicationController
  before_action :profile_resource
  before_action :require_owner, only: %i[edit update]

  def show
    @user = User.find(params[:user_id])
  end

  def edit; end

  def update
    @profile.update(profile_params)
    if @profile.save
      redirect_to user_profile_path
    else
      render :edit
    end
  end

  private

  def profile_resource
    @profile = User.find(params[:user_id]).profile
  end

  def profile_params
    params.require(:profile).permit(:first_name, :last_name, :username, :birthday, :phone, :avatar, :about)
  end

  def require_owner
    redirect_to root_path unless current_user == @profile.user
  end
end
