# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :banned?

  def banned?
    return unless current_user.present? && current_user.banned?

    sign_out current_user
    redirect_to banned_path
  end

  private

  def require_user
    redirect_to root_path unless current_user
  end
end
