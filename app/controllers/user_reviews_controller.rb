class UserReviewsController < ApplicationController
  before_action :user_review_resource, only: %i[create destroy]

  def index
    @user_review = UserReview.new
    @user_reviews = UserReview.where(commenter_id: params[:user_id]).page(params[:page])
  end

  def create
    @user_review = @user.user_reviews.create(user_review_params)

    redirect_to :back
  end

  def destroy
    @user.user_reviews.find(params[:id]).destroy!

    redirect_to :back
  end

  private

  def user_review_resource
    @user = User.find(params[:user_id])
  end

  def user_review_params
    params.require(:user_review).permit(:commenter_id, :body, :type_reviews)
  end
end
