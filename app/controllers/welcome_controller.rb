# frozen_string_literal: true

class WelcomeController < ApplicationController
  def index
    @categories = Category.without_parent
    @ads = Ad.published.latest.limit(6)
    @users = User.latest_by_ads.limit(5)
  end

  def chat_user
    session[:chat_rooms] ||= []
 
    @users = User.all.where.not(id: current_user)
    @chat_rooms = ChatRoom.includes(:recipient, :latters)
                                 .find(session[:chat_rooms])
  end
end
