class LattersController < ApplicationController
  def create
    @chat_room = ChatRoom.includes(:recipient).find(params[:chat_room_id])
    @latter = @chat_room.latters.create(latter_params)

    respond_to do |format|
      format.js
    end
  end

  private

  def latter_params
    params.require(:latter).permit(:user_id, :body)
  end
end
