# frozen_string_literal: true

module ProfilesHelper
  def require_user_profile
    current_user == @profile.user
  end

  def user_admin?
    if @profile.user.admin?
      content_tag(:p, 'Admin')
    else
      content_tag(:p, 'User')
    end
  end
end
