# frozen_string_literal: true

class MessageMailer < ApplicationMailer
  default from: 'Hello'
  default to: 'gtafan51@gmail.com'

  def new_message(message)
    @message = message

    mail subject: "Message from #{message.name}, #{message.email}"
  end
end
