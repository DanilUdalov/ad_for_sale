//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require nested_form_fields
//= require social-share-button
//= require trix
//= require_tree .
$(function() {
  setTimeout(function(){
    $('.latter').hide('fade');
  }, 3000);
});
(function() {
  $(document).on('click', '.toggle-window', function(e) {
    e.preventDefault();
    var panel = $(this).parent().parent();
    var latters_list = panel.find('.latters-list');

    panel.find('.panel-body').toggle();
    panel.attr('class', 'panel panel-default');

    if (panel.find('.panel-body').is(':visible')) {
      var height = latters_list[0].scrollHeight;
      latters_list.scrollTop(height);
    }
  });
})();
