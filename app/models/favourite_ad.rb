# frozen_string_literal: true

class FavouriteAd < ApplicationRecord
  belongs_to :ad
  belongs_to :user

  def self.manage(ad)
    object = find_by(ad: ad)
    object ? object.destroy : create(ad: ad)
  end
end
