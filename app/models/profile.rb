# frozen_string_literal: true

class Profile < ApplicationRecord
  belongs_to :user

  mount_uploader :avatar, AvatarUploader

  phony_normalize :phone

  validates :phone, phony_plausible: true

  def full_name
    "#{first_name} #{last_name}"
  end
end
