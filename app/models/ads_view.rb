# frozen_string_literal: true

class AdsView < ApplicationRecord
  belongs_to :ad
  belongs_to :user

  validates :user_id, uniqueness: { scope: :ad_id }
end
