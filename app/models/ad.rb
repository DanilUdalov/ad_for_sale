# frozen_string_literal: true

class Ad < ApplicationRecord
  belongs_to :user, counter_cache: true

  has_and_belongs_to_many :categories

  has_many :ads_views, dependent: :delete_all
  has_many :favourite_ads, dependent: :delete_all
  has_many :images, as: :imageable, dependent: :delete_all

  scope :latest, -> { order('created_at DESC') }
  scope :published, -> { where(published: true) }
  scope :not_published, -> { where(published: false) }

  accepts_nested_attributes_for :images, allow_destroy: true

  mount_uploader :image_ad, ImageAdUploader

  self.per_page = 10

  def self.search(search)
    where('name LIKE ?', "%#{search}%")
  end
end
