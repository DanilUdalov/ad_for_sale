# frozen_string_literal: true

class AdsCategories < ApplicationRecord
  belongs_to :ad
  belongs_to :category
end
