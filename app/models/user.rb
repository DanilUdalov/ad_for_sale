# frozen_string_literal: true

class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,  :omniauthable, :omniauth_providers => [:facebook]

  has_many :categories
  has_many :ads, dependent: :destroy
  has_many :ads_views, dependent: :destroy
  has_many :favourite_ads, dependent: :destroy
  has_many :user_reviews, dependent: :destroy

  has_many :latters, dependent: :destroy
  has_many :chat_rooms, foreign_key: :sender_id

  has_one :profile, dependent: :destroy

  scope :latest_by_ads, -> { order(ads_count: :desc) }

  before_create :build_profile

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      binding.pry
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      # user.profile.first_name = auth.info.name.split(' ').first
      # user.profile.last_name = auth.info.name.split(' ').second
    end
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"] if user.email.blank?
      end
    end
  end

  def name_space
    if profile.full_name.delete(' ').length.zero?
      email
    else
      profile.full_name
    end
  end
end
