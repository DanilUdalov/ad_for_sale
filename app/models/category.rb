# frozen_string_literal: true

class Category < ApplicationRecord
  has_many :subcategories, class_name: 'Category',
                           foreign_key: :parent_category_id,
                           dependent: :destroy

  has_and_belongs_to_many :ads, dependent: :delete_all

  has_one :parent, class_name: 'Category',
                   primary_key: :parent_category_id, foreign_key: :id

  accepts_nested_attributes_for :subcategories, allow_destroy: true

  scope :without_parent, -> { where(parent_category_id: nil) }
  scope :without_subcategory, -> { where('parent_category_id IS NOT NULL') }
end
