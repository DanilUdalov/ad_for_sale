class UserReview < ApplicationRecord
  belongs_to :user
  belongs_to :commenter, class_name: 'User', foreign_key: 'commenter_id'

  self.per_page = 10
end
