# frozen_string_literal: true

FactoryGirl.define do
  factory :profile do
    first_name Faker::Name.first_name
    last_name Faker::Name.last_name
    username Faker::Internet.user_name
    birthday Faker::Date.forward(23)
    avatar do
      File.new(Rails.root.join('spec', 'fixtures', 'images', 'test.jpg'))
    end
    about Faker::Lorem.sentence
    user
  end
end
