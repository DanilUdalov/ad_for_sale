# frozen_string_literal: true

FactoryGirl.define do
  factory :ad do
    name { Faker::Company.name }
    price Faker::Number.number(100)
    description Faker::Lorem.sentence
    image_ad do
      File.new(Rails.root.join('spec', 'fixtures', 'images', 'test.jpg'))
    end
    user
  end

  trait :published do
    published true
  end

  trait :with_user do
    user
  end
end
