FactoryGirl.define do
  factory :user_review do
    user
    commenter
    body Faker::Lorem.sentence(3)
  end
end
