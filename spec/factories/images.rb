# frozen_string_literal: true

FactoryGirl.define do
  factory :image do
    url do
      File.new(Rails.root.join('spec', 'fixtures', 'images', 'test.jpg'))
    end
  end
end
