# frozen_string_literal: true

FactoryGirl.define do
  factory :favourite_ad do
    ad
    user
  end
end
