# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MessagesController, type: :controller do
  let!(:message) { Message.new(name: 'Hello', email: 'qaz@mail.ru', content: 'Loren inpum') }
  before { get :new }

  describe '#new' do
    subject { response }

    it { is_expected.to have_http_status :ok }
    it { is_expected.to render_template(:new) }
  end

  describe '#create' do
    context 'valid data' do
      it { expect(message.name).to eq('Hello') }
      it { expect(message.email).to eq('qaz@mail.ru') }
      it { expect(message.content).to eq('Loren inpum') }
      it { expect(response).to have_http_status(200) }
    end

    context 'invalid data' do
      it { expect(response).to render_template(:new) }
      it { expect { raise 'An error occurred while delivering this message.' }.to raise_error }
    end
  end
end
