require 'rails_helper'

describe 'Message', type: :feature do
  let!(:message) { Message.new(name: 'Hello', email: 'qaz@mail.ru', content: 'Loren inpum') }

  scenario 'Contact forms' do
    visit contact_path

    fill_in 'Name', with: 'Hello'
    fill_in 'Email', with: 'qaz@mail.ru'
    fill_in 'Content', with: 'Loren inpum'

    click_button('Send message')

    expect(page).to have_content('Your messages has been sent.')
  end
end
