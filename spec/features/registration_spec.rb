require 'rails_helper'

describe 'Registration', type: :feature do
  let!(:user) { FactoryGirl.create(:user) }

  before do
    visit new_user_registration_path
  end

  scenario 'Registration user' do
    fill_in 'Email', with: Faker::Internet.email
    fill_in 'Password', with: 'password'
    fill_in 'Password confirmation', with: 'password'
    click_button('Sign up')

    expect(page).to have_content 'Edit profile'
  end
end
