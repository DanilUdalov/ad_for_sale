require 'rails_helper'

describe 'Sessions', type: :feature do
  let!(:user) { FactoryGirl.create(:user) }

  before do
    visit new_user_session_path
  end

  scenario 'Login user' do
    fill_in 'Email', with: Faker::Internet.email
    fill_in 'Password', with: 'password'
    click_button('Log in')

    expect(page).to have_content 'Success'
  end

  scenario 'Invalid login' do
    fill_in 'Email', with: ''
    fill_in 'Password', with: ''
    click_button('Log in')

    expect(page).to have_content 'Invalid Email or password.'
  end
end
