require 'rails_helper'

describe 'Profile' do
  let!(:user) { FactoryGirl.create(:user) }
  let!(:profile) { FactoryGirl.create(:profile) }

  before do
    allow_any_instance_of(DeviseController).to receive(:current_user).and_return(user)
  end

  scenario 'Update profile' do
    visit edit_user_profile_path(profile.id, user.id)

    fill_in 'First name', with: 'Bart'
    fill_in 'Last name', with: 'Simpson'
    fill_in 'Username', with: 'Caramba'
    fill_in 'Birthday', with: Faker::Date.forward(23)
    fill_in 'Phone', with: '+380504340342'
    fill_in 'About', with: 'Hello world'

    expect(page).to have_content 'Bart Simpson'
  end
end
