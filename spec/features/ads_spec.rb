require 'rails_helper'

describe 'Ads', type: :feature do
  let(:user) { attributes_for(:user) }
  let(:ad) { FactoryGirl.create(:ad) }

  before do
    allow_any_instance_of(DeviseController).to receive(:current_user).and_return(user)
  end

  scenario 'Show ad' do
    visit ad_path(ad)

    expect(page).to have_content('My ads')
  end

  scenario 'New ad' do
    visit new_ad_path

    fill_in 'Name', with: Faker::Company.name
    fill_in 'Price', with: Faker::Number.number(100)
    fill_in 'Description', with: Faker::Lorem.sentence

    click_on('Create Ad')

    expect(page).to have_content('My ads')
  end
end
