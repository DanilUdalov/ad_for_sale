# frozen_string_literal: true

require 'rails_helper'

RSpec.describe FavouriteAd, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:ad) }
    it { is_expected.to belong_to(:user) }
  end

  describe '.manage' do
    context 'when object is exist' do
      let!(:favourite_ad) { create(:favourite_ad) }

      it 'destroys object' do
        expect {
          described_class.manage(favourite_ad.ad)
        }.to change { described_class.count }.from(1).to(0)
      end
    end

    context 'when object is new' do
      let!(:ad) { create(:ad) }
      let(:user) { create(:user) }

      it 'creates object' do
        expect {
          user.favourite_ads.manage(ad)
        }.to change { described_class.count }.from(0).to(1)
      end
    end    
  end
end
