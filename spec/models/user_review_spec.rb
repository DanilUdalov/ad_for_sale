require 'rails_helper'

RSpec.describe UserReview, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:user) }

    it do
      is_expected.to belong_to(:commenter)
        .class_name('User')
        .with_foreign_key(:commenter_id)
    end
  end
end
