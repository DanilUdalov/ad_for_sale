# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Ad, type: :model do
  describe 'associations' do
    it { is_expected.to belong_to(:user) }

    it { is_expected.to have_and_belong_to_many(:categories) }

    it { is_expected.to have_many(:ads_views).dependent(:delete_all) }
    it { is_expected.to have_many(:favourite_ads).dependent(:delete_all) }
    it { is_expected.to have_many(:images).dependent(:delete_all) }
  end

  describe '.published' do
    let(:ad) { create(:ad) }
    let(:published_ad) { create(:ad, :published) }

    it 'returns published ads' do
      expect(described_class.published).to eq [published_ad]
    end
  end

  describe '.not_published' do
    let(:ad) { create(:ad) }
    let(:published_ad) { create(:ad, :published) }

    it 'returns not published ads' do
      expect(described_class.not_published).to eq [ad]
    end
  end

  describe '.search' do
    let(:ads) { create_list(:ad, 2) }

    it 'returns searched ads by name' do
      result = described_class.search(ads.first.name)

      expect(result).to eq [ads.first]
    end
  end
end
