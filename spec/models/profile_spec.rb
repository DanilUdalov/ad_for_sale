# frozen_string_literal: true

require 'rails_helper'
require 'devise'

RSpec.describe Profile, type: :model do
  describe 'associations' do
    it { belong_to(:user) }
  end

  describe '#full_name' do
    let(:profile) { create(:profile) }

    it 'returns full name' do
      expect(profile.full_name).to eq ("#{profile.first_name} #{profile.last_name}")
    end
  end
end
