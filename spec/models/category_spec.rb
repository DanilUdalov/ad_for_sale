# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Category, type: :model do
  describe 'associations' do
    it { is_expected.to have_and_belong_to_many(:ads) }

    it do
      is_expected.to have_many(:subcategories)
        .class_name('Category')
        .dependent(:destroy)
        .with_foreign_key(:parent_category_id)
    end

    it do
      is_expected.to have_one(:parent)
        .class_name('Category')
        .with_primary_key(:parent_category_id)
        .with_foreign_key(:id)
    end
  end
end
