# frozen_string_literal: true

require 'rails_helper'
require 'devise'

RSpec.describe User, type: :model do
  let(:user) { FactoryGirl.create(:user) }
  let(:profile) { FactoryGirl.create(:profile, user_id: user.id) }

  describe 'associations' do
    it { have_many(:ads) }
    it { have_many(:ads_views) }
    it { have_many(:categories) }
    it { have_one(:favourite_ads) }
    it { have_one(:profile) }
  end

  describe 'validations' do
    it { have_secure_token }
    it { have_secure_password }
  end
end
