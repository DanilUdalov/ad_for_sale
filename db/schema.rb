# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170822082558) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "ads", force: :cascade do |t|
    t.string   "name"
    t.integer  "category_id"
    t.integer  "user_id"
    t.float    "price"
    t.text     "description"
    t.string   "image_ad"
    t.boolean  "published",   default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["category_id"], name: "index_ads_on_category_id", using: :btree
    t.index ["user_id"], name: "index_ads_on_user_id", using: :btree
  end

  create_table "ads_categories", id: false, force: :cascade do |t|
    t.integer "category_id"
    t.integer "ad_id"
    t.index ["ad_id"], name: "index_ads_categories_on_ad_id", using: :btree
    t.index ["category_id"], name: "index_ads_categories_on_category_id", using: :btree
  end

  create_table "ads_views", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "ad_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ad_id"], name: "index_ads_views_on_ad_id", using: :btree
    t.index ["user_id"], name: "index_ads_views_on_user_id", using: :btree
  end

  create_table "categories", force: :cascade do |t|
    t.integer  "parent_category_id"
    t.string   "name",               null: false
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "chat_rooms", force: :cascade do |t|
    t.integer  "recipient_id"
    t.integer  "sender_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["recipient_id", "sender_id"], name: "index_chat_rooms_on_recipient_id_and_sender_id", unique: true, using: :btree
  end

  create_table "favourite_ads", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "ad_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ad_id"], name: "index_favourite_ads_on_ad_id", using: :btree
    t.index ["user_id"], name: "index_favourite_ads_on_user_id", using: :btree
  end

  create_table "images", force: :cascade do |t|
    t.string   "url"
    t.string   "imageable_type"
    t.integer  "imageable_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["imageable_type", "imageable_id"], name: "index_images_on_imageable_type_and_imageable_id", using: :btree
  end

  create_table "latters", force: :cascade do |t|
    t.text     "body"
    t.integer  "user_id"
    t.integer  "chat_room_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["chat_room_id"], name: "index_latters_on_chat_room_id", using: :btree
    t.index ["user_id"], name: "index_latters_on_user_id", using: :btree
  end

  create_table "profiles", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "username"
    t.string   "phone"
    t.date     "birthday"
    t.string   "avatar"
    t.integer  "user_id"
    t.text     "about"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_profiles_on_user_id", using: :btree
  end

  create_table "user_reviews", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "commenter_id"
    t.boolean  "type_reviews"
    t.text     "body"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "admin",                  default: false
    t.integer  "ads_count",              default: 0
    t.boolean  "banned",                 default: false
    t.string   "provider"
    t.string   "uid"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "ads", "categories"
  add_foreign_key "ads", "users"
  add_foreign_key "ads_views", "ads"
  add_foreign_key "ads_views", "users"
  add_foreign_key "favourite_ads", "ads"
  add_foreign_key "favourite_ads", "users"
  add_foreign_key "latters", "chat_rooms"
  add_foreign_key "latters", "users"
  add_foreign_key "profiles", "users"
end
