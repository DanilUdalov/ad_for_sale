Category.create(name: 'The property')
c1 = Category.last
c1.subcategories.create([{ name: 'At home' }, { name: 'Apartments' },
                         { name: 'Rooms' }, { name: 'Premises' }])
Category.create(name: 'Electronic')
c2 = Category.last
c2.subcategories.create([{ name: 'TV' }, { name: 'Phone' },
                         { name: 'Computers' }, { name: 'Audiotechnik' },
                         { name: 'Photo/Video' }, { name: 'Home Appliances' }])
Category.create(name: 'Pets')
c3 = Category.last
c3.subcategories.create([{ name: 'Cats' }, { name: 'Dogs' },
                         { name: 'Birds' }, { name: 'Aquarium' },
                         { name: 'Rodents' }, { name: 'Reptiles' }])
Category.create(name: 'Auto')
c4 = Category.last
c4.subcategories.create([{ name: 'Auto' }, { name: 'Moto' },
                         { name: 'Bus' }, { name: 'Track' },
                         { name: 'Bike' }, { name: 'Others' }])
