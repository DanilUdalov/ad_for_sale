class AddCounterCacheToUsers < ActiveRecord::Migration[5.0]
  def up
    add_column :users, :ads_count, :integer, default: 0

    User.reset_column_information
    User.all.each do |user|
      User.update_counters user.id, ads_count: user.ads.where(published: true).length
    end
  end
end
