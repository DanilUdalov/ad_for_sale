class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.string :username
      t.string :phone
      t.date :birthday
      t.string :avatar
      t.belongs_to :user, foreign_key: true
      t.text :about

      t.timestamps
    end
  end
end
