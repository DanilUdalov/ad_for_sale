class CreateAds < ActiveRecord::Migration[5.0]
  def change
    create_table :ads do |t|
      t.string :name
      t.belongs_to :category, foreign_key: true
      t.belongs_to :user, foreign_key: true
      t.float :price
      t.text :description
      t.string :image_ad
      t.boolean :published, default: false

      t.timestamps
    end
  end
end
