class CreateUserReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :user_reviews do |t|
      t.integer :user_id
      t.integer :commenter_id
      t.boolean :type_reviews
      t.text :body
      t.timestamps
    end
  end
end
