class CreateAdsCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :ads_categories, id: false do |t|
      t.belongs_to :category, index: true
      t.belongs_to :ad, index: true
    end
  end
end
